// Playing Cards
// Alexander Rosas-Cortez
// Mai Vue Xiong

#include <iostream>
#include <conio.h>

using namespace std;

enum class Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};
enum class Suit
{
	Clubs,
	Diamonds,
	Hearts,
	Spades
};


struct Card
{
	Rank Rank;
	Suit Suit;

};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{

	Card c1;
	c1.Rank = Rank::Ace;
	c1.Suit = Suit::Spades;

	Card c2;
	c2.Rank = Rank::Two;
	c2.Suit = Suit::Spades;

	cout << "Card1: ";
	PrintCard(c1);
	cout << "\nCard2: ";
	PrintCard(c2);

	cout << "\nHigh Card: ";
	PrintCard(HighCard(c1, c2));


	(void)_getch();
	return 0;
}

void PrintCard(Card card)
{
	switch (card.Rank) {
	case Rank::Two: cout << "The Two of "; break;
	case Rank::Three: cout << "The Three of "; break;
	case Rank::Four: cout << "The Four of "; break;
	case Rank::Five: cout << "The Five of "; break;
	case Rank::Six: cout << "The Six of "; break;
	case Rank::Seven: cout << "The Seven of"; break;
	case Rank::Eight: cout << "The Eight of "; break;
	case Rank::Nine: cout << "The Nine of "; break;
	case Rank::Ten: cout << "The Ten of"; break;
	case Rank::Jack: cout << "The Jack of "; break;
	case Rank::Queen: cout << "The Queen of "; break;
	case Rank::King: cout << "The King of "; break;
	case Rank::Ace: cout << "The Ace of "; break;
	}
	switch (card.Suit) {
	case Suit::Clubs: cout << "Clubs\n"; break;
	case Suit::Diamonds: cout << "Diamonds\n"; break;
	case Suit::Hearts: cout << "Hearts\n"; break;
	case Suit::Spades: cout << "Spades\n"; break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank) {
		return card1;
	}
	else if (card1.Rank < card2.Rank) {
		return card2;
	}
	else {
		if (card1.Suit > card2.Suit) {
			return card1;
		}
		else {
			return card2;
		}
	}
}